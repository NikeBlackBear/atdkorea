package atdkorea.loginsoft.atdkorea.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import atdkorea.loginsoft.atdkorea.Model.FarmListModel;
import atdkorea.loginsoft.atdkorea.R;

/**
 * Created by jun on 2015-11-25.
 */
public class FarmListAdapter extends BaseAdapter {
    private Activity mActivity;
    private ArrayList<FarmListModel> mlist;
    private LayoutInflater mInflater;
    public FarmListAdapter(Activity c, ArrayList<FarmListModel> list) {
        mActivity = c;
        mInflater = LayoutInflater.from(c);
        mlist = list;
    }
    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public FarmListModel getItem(int i) {
        return mlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = mInflater.inflate(R.layout.farm_list_row, null);
            holder = new ViewHolder();
            holder.farmName = (TextView)view.findViewById(R.id.farm_name);
            holder.num = (TextView)view.findViewById(R.id.num);
            holder.minenal = (TextView)view.findViewById(R.id.minenal);
            holder.state = (ImageView)view.findViewById(R.id.state);
        }else {
            holder = (ViewHolder)view.getTag();
        }
        view.setTag(holder);
        holder.farmName.setText(getItem(i).getFarmName());
        holder.num.setText(getItem(i).getAmount());
        holder.minenal.setText(getItem(i).getMineral());
        switch (Integer.parseInt(getItem(i).getState())){
            case 1:holder.state.setImageResource(R.drawable.red); break;
            case 2:holder.state.setImageResource(R.drawable.yellow); break;
            case 3:holder.state.setImageResource(R.drawable.blue); break;
        }

        return view;
    }
    public class ViewHolder {
        TextView farmName;
        TextView num;
        TextView minenal;
        ImageView state;
    }
}
