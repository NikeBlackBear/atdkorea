package atdkorea.loginsoft.atdkorea.Model;

/**
 * Created by jun on 2015-11-25.
 */
public class FarmListModel {
    private String FarmName;
    private String amount;
    private String mineral;
    private String state;

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setFarmName(String farmName) {
        FarmName = farmName;
    }

    public String getFarmName() {
        return FarmName;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setMineral(String mineral) {
        this.mineral = mineral;
    }

    public String getMineral() {
        return mineral;
    }
}
