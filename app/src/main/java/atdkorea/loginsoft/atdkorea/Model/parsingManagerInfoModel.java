package atdkorea.loginsoft.atdkorea.Model;

import java.util.ArrayList;

/**
 * Created by jun on 2015-11-25.
 */
public class parsingManagerInfoModel {
    private String success;
    private String message;
    private String total;
    private String date;
    private ArrayList<FarmListModel> messageinfo;

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<FarmListModel> getMessageinfo() {
        return messageinfo;
    }

    public void setMessageinfo(ArrayList<FarmListModel> messageinfo) {
        this.messageinfo = messageinfo;
    }

    public String getMessage() {
        return message;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal() {
        return total;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
