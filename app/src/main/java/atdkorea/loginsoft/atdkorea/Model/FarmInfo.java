package atdkorea.loginsoft.atdkorea.Model;

import java.io.Serializable;

/**
 * Created by Administrator on 2015-11-25.
 */
public class FarmInfo implements Serializable {
    public String success;
    public String message;
    public String state;
    public String FarmName;
    public String FarmCall;
    public String sunsi;
    public String juksan;
    public String toyak;
    public String pump;
    public String uryang;
    public String nujuk;
    public String yakpum;
    public String email;
}
