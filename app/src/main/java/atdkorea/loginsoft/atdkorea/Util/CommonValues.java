package atdkorea.loginsoft.atdkorea.Util;

/**
 * Created by jun on 2015-11-25.
 */
public interface CommonValues {
    public static final String SERVER_PORXY_PHONE_NUM = "user_phone"; // 폰 번호
    public static final String RESPONSE_SUCCESS = "1";
    public static final String RESPONSE_FAILED = "0";
}
