package atdkorea.loginsoft.atdkorea.Util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Administrator on 2015-11-25.
 */
public class ContactIntentFactory {
    private static ContactIntentFactory mInstance;
    private Context mContext;

    public static ContactIntentFactory getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ContactIntentFactory(context);
        }
        return mInstance;
    }

    private ContactIntentFactory(Context context) {
        mContext = context;
    }

    public void sendSMS(String phoneNumber) {
        sendSMS(phoneNumber, "");
    }

    public void sendSMS(String phoneNumber, String message) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        if (!message.equals("")) {
            smsIntent.putExtra("sms_body", message);
        }
        smsIntent.putExtra("address", phoneNumber);
        smsIntent.setType("vnd.android-dir/mms-sms");
        mContext.startActivity(smsIntent);
    }

    public void sendCall(String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        mContext.startActivity(callIntent);
    }

    public void sendEmail(String emailAddress) {
        mContext.startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + emailAddress)));
    }

}

