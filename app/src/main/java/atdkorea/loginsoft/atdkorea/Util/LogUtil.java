package atdkorea.loginsoft.atdkorea.Util;

/**
 * Created by jun on 2015-11-25.
 */
public class LogUtil {
    private final static String TAG = "ADTKorea";
    private final static boolean DEBUG = true;

    public static void LOGV(String msg) {
        if (DEBUG) {
            android.util.Log.v(TAG, msg);
        }
    }

    public static void LOGE(String msg) {
        if (DEBUG) {
            android.util.Log.e(TAG, msg);
        }
    }

    public static void LOGD(String msg) {
        if (DEBUG) {
            android.util.Log.d(TAG, msg);
        }
    }
}

