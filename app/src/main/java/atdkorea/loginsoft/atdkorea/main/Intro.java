package atdkorea.loginsoft.atdkorea.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import atdkorea.loginsoft.atdkorea.R;
import atdkorea.loginsoft.atdkorea.Server.ServerProxy;
import atdkorea.loginsoft.atdkorea.Util.LogUtil;


public class Intro extends Activity {
    private Button managerBtn, farmmerBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        init();
    }
    public void init(){
        managerBtn = (Button)findViewById(R.id.manager_btn);
        farmmerBtn = (Button)findViewById(R.id.farmmer_btn);
        managerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intro.this,Manager.class);
                startActivity(intent);
            }
        });
        farmmerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intro.this, Farmmer.class);
                startActivity(intent);
            }
        });

//        TelephonyManager telManager = (TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE);
//        String phoneNum = telManager.getLine1Number();
//        ServerProxy.getInstance(this).distinctionUser(distinctionUserCallback, phoneNum);
    }
//    private ServerProxy.ResponseCallback distinctionUserCallback = new ServerProxy.ResponseCallback() {
//        @Override
//        public void onResponse(String response) {
//            if (Intro.this != null) {
//                LogUtil.LOGV("Intro :: requestUserInfoCallback :: onResponse() :: response = " + response);
//                UserInfoModel Modeltemp = new ParseManager().parseUserInfo(response);
//                LogUtil.LOGV("Intro :: requestNewFeedbackCodeCallback :: Modeltemp = " + Modeltemp.toString());
//                mUserInfoModel = Modeltemp;
//            } else {
//                LogUtil.LOGV("Intro :: requestNewFeedbackCodeCallback :: not exists next data");
//            }
//            if (mUserInfoModel.getMessageinfo().equals("No Search User")) {
//                Toast.makeText(getActivity(), "" + R.string.friend_list_no_id, Toast.LENGTH_SHORT).show();
//            } else {
//                Intent intent = new Intent(getActivity(), MyProfile.class);
//                intent.putExtra("userFlag", 2);
//                intent.putExtra("FeedbackCode", inputTextView);
//                startActivity(intent);
//            }
//        }
//
//        @Override
//        public void onError(int status) {
//            LogUtil.LOGV("Intro :: onError() :: status =" + status);
//        }
//    };

}
