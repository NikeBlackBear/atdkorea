package atdkorea.loginsoft.atdkorea.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import atdkorea.loginsoft.atdkorea.Model.FarmInfo;
import atdkorea.loginsoft.atdkorea.R;
import atdkorea.loginsoft.atdkorea.Server.ParseManager;
import atdkorea.loginsoft.atdkorea.Server.ServerProxy;
import atdkorea.loginsoft.atdkorea.Util.CommonValues;
import atdkorea.loginsoft.atdkorea.Util.ContactIntentFactory;
import atdkorea.loginsoft.atdkorea.Util.LogUtil;

public class Farmmer extends Activity implements CommonValues {

    private final int mBottomBtnIds[] = {R.id.btn_message, R.id.btn_call, R.id.btn_mail, R.id.btn_download};

    private ImageView mStatusIcon;
    private TextView mFarmName;
    private TextView mModeValue;
    private TextView mSunsiValue;
    private TextView mJukSanValue;
    private TextView mTuyakValue;
    private TextView mPumpValue;
    private TextView mUrangValue;
    private TextView mNujukValue;
    private TextView mJanrangValue;
    private ImageView mJanrangIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmmer);
        init();
    }

    private void init() {
        mStatusIcon = (ImageView) findViewById(R.id.status_icon);
        mFarmName = (TextView) findViewById(R.id.farm_name);
        mModeValue = (TextView) findViewById(R.id.mode_value_1);
        mSunsiValue = (TextView) findViewById(R.id.sunsi_value);
        mJukSanValue = (TextView) findViewById(R.id.juksan_value);
        mTuyakValue = (TextView) findViewById(R.id.tuyak_value);
        mPumpValue = (TextView) findViewById(R.id.pump_value);
        mUrangValue = (TextView) findViewById(R.id.yurang_value);
        mNujukValue = (TextView) findViewById(R.id.nujuk_value);
        mJanrangValue = (TextView) findViewById(R.id.janrang_value);
        mJanrangIcon = (ImageView) findViewById(R.id.janrang_icon);

        for (int i = 0; i < mBottomBtnIds.length; i++) {
            findViewById(mBottomBtnIds[i]).setOnClickListener(mFammerBottomBtnListener);
        }

//        TelephonyManager telManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
//        telManager.getLine1Number();
        ServerProxy.getInstance(this).getFarmerInfo(mGetFarmerInfoResponseCallback, "01026966525");
    }

    private void setupData(FarmInfo info) {
        mFarmName.setText(info.FarmName);
        mModeValue.setText(info.state);
        mSunsiValue.setText(info.sunsi);
        mJukSanValue.setText(info.juksan);
        mTuyakValue.setText(info.toyak);
        mPumpValue.setText(info.pump);
        mUrangValue.setText(info.uryang);
        mNujukValue.setText(info.nujuk);
        mJanrangValue.setText(info.yakpum + "%");

        int yakpumIntValue = Integer.parseInt(info.yakpum);
        if (yakpumIntValue < 20) {
            mStatusIcon.setImageResource(R.drawable.icon_stop);
            mJanrangIcon.setImageResource(R.drawable.battery_0);
        } else if (yakpumIntValue < 30) {
            mStatusIcon.setImageResource(R.drawable.icon_stop);
            mJanrangIcon.setImageResource(R.drawable.battery_20);
        } else if (yakpumIntValue < 50) {
            mStatusIcon.setImageResource(R.drawable.icon_alarm);
            mJanrangIcon.setImageResource(R.drawable.battery_30);
        } else if (yakpumIntValue < 60) {
            mStatusIcon.setImageResource(R.drawable.icon_alarm);
            mJanrangIcon.setImageResource(R.drawable.battery_50);
        } else if (yakpumIntValue < 80) {
            mStatusIcon.setImageResource(R.drawable.icon_alarm);
            mJanrangIcon.setImageResource(R.drawable.battery_60);
        } else if (yakpumIntValue < 90) {
            mStatusIcon.setImageResource(R.drawable.icon_run);
            mJanrangIcon.setImageResource(R.drawable.battery_80);
        } else if (yakpumIntValue < 100) {
            mStatusIcon.setImageResource(R.drawable.icon_run);
            mJanrangIcon.setImageResource(R.drawable.battery_90);
        } else {
            mStatusIcon.setImageResource(R.drawable.icon_run);
            mJanrangIcon.setImageResource(R.drawable.battery_100);
        }
    }

    private final ServerProxy.ResponseCallback mGetFarmerInfoResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("Farmmer :: mGetFarmerInfoResponseCallback :: response = " + response);
            FarmInfo info = new ParseManager().parseFarmInfo(response);
            if (info.success.equals(RESPONSE_SUCCESS)) {
                setupData(info);
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGE("Farmmer :: mGetFarmerInfoResponseCallback :: onError :: status = " + status);
        }
    };

    private final View.OnClickListener mFammerBottomBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_message:
                    ContactIntentFactory.getInstance(Farmmer.this).sendSMS("01001010101");
                    break;

                case R.id.btn_call:
                    ContactIntentFactory.getInstance(Farmmer.this).sendCall("01001010101");
                    break;

                case R.id.btn_mail:
                    ContactIntentFactory.getInstance(Farmmer.this).sendEmail("testtesttt@gmail.com");
                    break;

                case R.id.btn_download:
                    break;
            }
        }
    };
}