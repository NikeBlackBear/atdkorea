package atdkorea.loginsoft.atdkorea.main;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import atdkorea.loginsoft.atdkorea.Adapter.FarmListAdapter;
import atdkorea.loginsoft.atdkorea.Model.parsingManagerInfoModel;
import atdkorea.loginsoft.atdkorea.R;
import atdkorea.loginsoft.atdkorea.Server.ParseManager;
import atdkorea.loginsoft.atdkorea.Server.ServerProxy;
import atdkorea.loginsoft.atdkorea.Util.LogUtil;
import atdkorea.loginsoft.atdkorea.Util.dateForm;

public class Manager extends Activity {
    private ListView farmList;
    private TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        init();
    }
    private void init(){
        farmList = (ListView)findViewById(R.id.farm_list);
        date = (TextView)findViewById(R.id.date);
        TelephonyManager telManager = (TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE);
        String phoneNum = telManager.getLine1Number();
        ServerProxy.getInstance(Manager.this).getMangerInfo(getMangerInfoCallback, "01026966525");
    }
    private ServerProxy.ResponseCallback getMangerInfoCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            if (Manager.this != null) {
                LogUtil.LOGV("Manager :: getMangerInfoCallback :: onResponse() :: response = " + response);
                parsingManagerInfoModel Modeltemp = new ParseManager().parseGetManagerModel(response);
                FarmListAdapter farmListAdapter = new FarmListAdapter(Manager.this, Modeltemp.getMessageinfo());
                date.setText((new dateForm()).tranformDate(Modeltemp.getDate()));
                farmList.setAdapter(farmListAdapter);
            } else {
                LogUtil.LOGV("Manager :: getMangerInfoCallback :: not exists next data");
            }
        }

        @Override
        public void onError(int status) {
            LogUtil.LOGV("Manager :: getMangerInfoCallback :: onError() :: status =" + status);
        }
    };
}
