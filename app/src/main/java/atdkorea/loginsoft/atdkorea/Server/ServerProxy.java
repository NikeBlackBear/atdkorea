package atdkorea.loginsoft.atdkorea.Server;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import atdkorea.loginsoft.atdkorea.Util.CommonValues;
import atdkorea.loginsoft.atdkorea.Util.LogUtil;

/**
 * Created by jun on 2015-11-25.
 */
public class ServerProxy implements CommonValues, RequestURL {
    private static ServerProxy mInstance;
    private RequestQueue mRequestQueue;
    private Context mContext;

    private ServerProxy(Context context) {
        mContext = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static ServerProxy getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ServerProxy(context);
        }
        return mInstance;
    }


    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    public interface ResponseCallback {
        void onResponse(String response);

        void onError(int status);
    }

    // 관리자 정보 가져오기
    public void getMangerInfo(final ResponseCallback callback, final String phonenum) {
        LogUtil.LOGE("ServerProxy :: getUser() :: phonenum = " + phonenum);

        StringRequest request = new StringRequest(Request.Method.POST, URL_DISTINCTION_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(SERVER_PORXY_PHONE_NUM, phonenum);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    // 농장 정보 가져오기
    public void getFarmerInfo(final ResponseCallback callback, final String phonenum) {
        LogUtil.LOGE("ServerProxy :: getFarmerInfo() :: phonenum = " + phonenum);

        StringRequest request = new StringRequest(Request.Method.POST, URL_GET_FARM_INFO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                if (e != null && e.networkResponse != null)
                    callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(
                    NetworkResponse response) {
                try {
                    String string = new String(response.data, "UTF-8");
                    return Response.success(string, HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(SERVER_PORXY_PHONE_NUM, phonenum);

                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }
}
