package atdkorea.loginsoft.atdkorea.Server;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import atdkorea.loginsoft.atdkorea.Model.FarmInfo;
import atdkorea.loginsoft.atdkorea.Model.parsingManagerInfoModel;
import atdkorea.loginsoft.atdkorea.Util.CommonValues;
import atdkorea.loginsoft.atdkorea.Util.LogUtil;

/**
 * Created by jun on 2015-11-25.
 */
public class ParseManager implements CommonValues {
    public ParseManager() {
    }

    public parsingManagerInfoModel parseGetManagerModel(String response) {
        LogUtil.LOGV("ParseManager :: parseGetManagerModel :: response = " + response);
        parsingManagerInfoModel temp;
        Gson gson = new Gson();
        temp = gson.fromJson(response, new TypeToken<parsingManagerInfoModel>() {
        }.getType());
        return temp;
    }

    public FarmInfo parseFarmInfo(String response) {
        return new Gson().fromJson(response, FarmInfo.class);
    }
}